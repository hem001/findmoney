var request = require('request');
var apiOptions = {
    server: "http://localhost:3000"
};
var sendJsonResponse= (res, status, content)=>{
    res.status(status);
    res.json(content);
};

var showError = function(req, res, status){
    var title, content;
  if (status === 404) {
    title = "404, page not found";
    content = "Looks like we can't find this page. Sorry.";
  } else if (status === 500) {
    title = "500, internal server error";
    content = "There's a problem with our server.";
  } else {
    title = status + ", something's gone wrong";
    content = "Something, somewhere, has gone just a little bit wrong.";
  }
  res.status(status);
  res.render('error_page', {
    title : title,
    content : content
  });
};


module.exports.getBlogById = (req, res) => {
    res.render('blog',{
        title: 'Blog posts',
        pageHeader:{
            title: 'Find Money',
            description: 'here you can find money'
        }
        ,
        
        blogs:[
        {
            title: "this is title",
            description: "this is description" ,
            content: "hello there this is description"
        },
            {
            title: "this is title1",
            description: "this is description" ,
            content: "hello there this is description"
        },
        {
            title: "this is title2",
            description: "this is description" ,
            content: "hello there this is description"
        }
        ]
    });


};

module.exports.newBlogPage=function(req, res){
    res.render( 'newBlogPage', {title:'Create New Blog'});
};

module.exports.uploadNewBlog = function(req, res){
    var requestOptions, path;
    path = '/api/blog/new';

    var postdata = {
        title: req.body.title,
        description: req.body.description,
        content: req.body.content
    };
    requestOptions ={
        url: apiOptions.server + path,
        method: "POST",
        json: postdata
    };
    request(
        requestOptions,
        function(err, response, body){
            if(response.statusCode === 200){
                res.redirect('/blog');
                console.log(body);
            }else{
                sendJsonResponse(res, 404, 'Error');
            }
        }
    );


};

module.exports.deleteBlogById = (req,res)=>{
    // var requestOptions, path;
    
    showError(req, res, 400);
    // sendJsonResponse(res, 200, "hello");

    // path = 'api/blog/' + req.params.blogid + '/delete';
};
module.exports.updateBlogById = (req,res)=>{
    sendJsonResponse(res, 200, "hello");
};
