var express = require('express');
var router = express.Router();
var homeCtrl = require('../controllers/home');
var blogCtrl = require('../controllers/blog');
var contactsCtrl = require('../controllers/contacts');
var aboutCtrl = require('../controllers/about');


/* GET home page. */

router.get('/', homeCtrl.angularApp);

// router.get('/', homeCtrl.index);

// router.get('/blog', blogCtrl.getBlogById);
// router.get('/blog/:blogid', blogCtrl.getBlogById);

// router.get('/blog/:blogid/new', blogCtrl.newBlogPage);
// router.post('/blog/:blogid/new', blogCtrl.uploadNewBlog);

// router.post('/blog/:blogid/delete', blogCtrl.deleteBlogById);
// router.post('/blog/:blogid/update', blogCtrl.updateBlogById);


// contacts
// router.get('/contacts', contactsCtrl.contacts);
// router.post('/contacts', contactsCtrl.addContact);

// router.get('/about', aboutCtrl.about);




// router.post('/blog/new', blogCtrl.blog.addBlog);

module.exports = router;
