require('dotenv').config();

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var uglifyJs = require("uglify-js");
var fs = require('fs');

var passport = require('passport');
var helmet = require('helmet');

require('./app_api/models/db_connection');
require('./app_api/config/passport');


// var routes = require('./app_server/routes/index');
var routerApi = require('./app_api/routes/index');
// var usersRouter = require('./app_server/routes/users');

var app = express();

// //use helmet 
// app.use(helmet.hidePoweredBy({setTo: 'PHP 4.2.0'}));
// //to stop others from adding our page as iframes
// app.use(helmet.frameguard({action: 'deny'}));
// //to protect from Cross-site-Scripting
// app.use(helmet.xssFilter());
//protect from untrusted HTML download
app.use(helmet.ieNoOpen());
//protect from protocol downgrade attacks and cookie hijacking
var ninetyDaysInSeconds = 90*24*60*60;
app.use(helmet.hsts({maxAge: ninetyDaysInSeconds, force:true}));

//to include noCache() and contentSecurityPolicy()
app.use(helmet({
  frameguard: false,
  noCache: true,
  contentSecurityPolicy: {   // enable and configure
   directives: {
     defaultSrc: ["'self'"],
    imgSrc: ["'self'", 'data:'],
    styleSrc: ["'self'", "'unsafe-inline'"],
   }
  },
 dnsPrefetchControl: false   // disable
}));


// view engine setup
// app.set('views', path.join(__dirname,'app_server', 'views'));
// app.set('view engine', 'pug');
var multer = require('multer');

// SET STORAGE
var storage = multer.diskStorage({
  destination: './uploads/blog_pics/',
  filename: function (req, file, cb) {
    // console.log('-----',file);
    console.log('-----',req.body.filename);
    // console.log('-----',res);

    cb(null, req.body.filename);
    // cb(null, file.originalname.replace(path.extname(file.originalname), "") + '-' + Date.now() + path.extname(file.originalname))
  }
});

var upload = multer({ storage: storage });
app.post('/uploadImages', upload.single('file'), function(req,res,next){
    // console.log('Uploade Successful ',req.file.filename);
    if(!req.file){
      res.json("error");
    }
    else{
      res.json("Successfully uploaded file");

    }
});
app.delete('/deleteImage',  function (req,res) {
    // console.log('body of delete is ', req.query.image_source);
    var filePath = './uploads/blog_pics/'+ req.query.image_source;

    fs.stat(filePath, function(err,stats){
      if(err){
        console.log('Error deleting the file'. err);
    return;
      } 
  
    fs.unlink(filePath, function(err){
      if(err){
        return console.log('Error deleting the file');
      } else{
        console.log('Successfully deleted the file');
      }
    });
    });
});



var appClientFiles = {

  "routes.js": fs.readFileSync("app_client/routes.js", "utf8"),
  "home.controller.js": fs.readFileSync("app_client/controllers/home.controller.js", "utf8"),
  "htmlLineBreaks.filter.js": fs.readFileSync("app_client/common/filters/addHtmlLineBreaks.filter.js", "utf8"),

//blog.controller.js cause of problem while minifying
//uglifyjs does not support arrow functions
  "blog.controller.js": fs.readFileSync("app_client/controllers/blog.controller.js", "utf8"),
  

  "blogData.js": fs.readFileSync("app_client/common/services/blogData.js", "utf8"),
  "navigation.directive.js": fs.readFileSync("app_client/common/directives/navigation/navigation.directive.js", "utf8"),
  "blog.cards.dirctive.js": fs.readFileSync("app_client/common/directives/blog-cards/blogCards.directive.js", "utf8"),
  "tip.directive.js": fs.readFileSync("app_client/common/directives/homepage-tips/homepage.tips.directive.js", "utf8"),
  "signInCtrl.js": fs.readFileSync('app_client/common/modal/signIn/signInCtrl.js', "utf8"),
  "authentication.service.js": fs.readFileSync("app_client/common/services/authentication.service.js", "utf8"),
  "authentication.controller.js": fs.readFileSync('app_client/controllers/authentication.controller.js', "utf8"),
  "navigation.controller.js": fs.readFileSync("app_client/common/directives/navigation/navigation.controller.js", "utf8"),
  "review.controller.js": fs.readFileSync("app_client/controllers/review.controller.js", "utf8"),
  "review.directive.js": fs.readFileSync("app_client/common/directives/review/review.directive.js", "utf8"),
  "review.directive.ctrl.js": fs.readFileSync("app_client/common/directives/review/review.directive.ctrl.js", "utf8"),
  "reply.directive.js": fs.readFileSync("app_client/common/directives/reply/reply.directive.js", "utf8"),
  "reply.directive.ctrl.js": fs.readFileSync("app_client/common/directives/reply/reply.directive.ctrl.js", "utf8"),
  "search-page.controller.js": fs.readFileSync("app_client/controllers/search-page.controller.js", "utf8"),

  "fileread.directive.js": fs.readFileSync("app_client/common/directives/file-directive/fileread.directive.js", "utf8"),
  "sessionStorage.factory.js": fs.readFileSync("app_client/common/factories/sessionStorage.factory.js", "utf8"),
  "cookies.modal.ctrl.js": fs.readFileSync("app_client/common/modal/cookies/cookies.modal.ctrl.js", "utf8"),
  "cookies.controller.js": fs.readFileSync("app_client/controllers/cookies.controller.js", "utf8"),
  "localStorage.factory.js.js": fs.readFileSync("app_client/common/factories/localStorage.factory.js", "utf8"), 
  "calc1.controller.js": fs.readFileSync("app_client/controllers/calc1.controller.js", "utf8"),

  // ".js": fs.readFileSync("app_client/.js", "utf8"),
  // ".js": fs.readFileSync("app_client/.js", "utf8"),




};


var uglified = uglifyJs.minify(appClientFiles, { compress: false });

fs.writeFile('public/angular/findMoney.min.js', uglified.code, function(err) {
  if (err) {
    console.log(err);
  } else {
    console.log("Script generated and saved:", 'findMoney.min.js');
  }
});


app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'app_client')));
app.use(express.static(path.join(__dirname, 'uploads')))
//needed for using /node_modules path
app.use(express.static(path.join(__dirname)));



app.use('/api', routerApi);
app.use(passport.initialize());


//route to app_client index.html
app.use(function(req, res) {
  res.sendFile(path.join(__dirname, 'app_client', 'index.html'));
});


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not found');
  err.status = 404;
  next(err);
});

//Catch unauthorised errors
app.use(function(err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({ "message": err.name + ": " + err.message });
  }
});



// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});



module.exports = app;
