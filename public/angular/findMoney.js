var blogData = function($http){
  return $http.get('/api/blog');

  // return [
  // {
  //   title: "this is title1",
  //   description: "this is description1" ,
  //   content: "hello there this is description"
  // },
  // {
  //   title: "this is title2",
  //   description: "this is description2" ,
  //   content: "hello there this is description"
  // },
  // {
  //   title: "this is title3",
  //   description: "this is description3" ,
  //   content: "hello there this is description"
  // },
  // {
  //   title: "this is title4",
  //   description: "this is description4" ,
  //   content: "hello there this is description"
  // }
  // ]
};


var blogCtrl = function($scope, blogData){
  $scope.d = 1000;
  blogData
    .success(function(data){
        $scope.data = {blog : data.blog,
          blogs: data.blogs};
      
    })
    .error(function (e) {
      console.log(e);
    });

};

angular
  .module('findMoneyApp',[])
  .controller('blogCtrl', blogCtrl)
  .service('blogData', blogData)
;