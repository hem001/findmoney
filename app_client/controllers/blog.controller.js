(function() {


  angular
    .module('findMoneyApp')
    .controller('blogCtrl', blogCtrl)
    .controller('blogByIdCtrl', blogByIdCtrl)
    .controller('newBlogCtrl', newBlogCtrl);
  blogCtrl.$inject = ['blogData', 'authentication', 'sessionStorage'];
  blogByIdCtrl.$inject = ['sessionStorage', 'blogDataById', '$routeParams', '$location', 'authentication'];



  function blogCtrl(blogData, authentication, sessionStorage) {
    var vm = this;
    vm.title = 'All Blog Page';
    vm.isLoggedIn = authentication.isLoggedIn();
    vm.errorMessage = "";

    // console.log('storage is ');
    vm.clearLocalStorage = function () {
      localStorage.clear();
    };
    vm.clearSessionStorage = function () {
      sessionStorage.clear();
    };

    var sessionBlogs = sessionStorage.getObject('sessionBlogList');
    if (!sessionBlogs || sessionBlogs.length == 0) {
      blogData.then((function(result) {
        vm.blogList = result.data.blogList;

        console.log('inside api call. bloglist is',vm.blogList);
	     
        sessionStorage.setObject("sessionBlogList");

      }), (function(e) {
        // console.log(e.data.message);
        vm.errorMessage = e.data.message;
      }));

    } else {
      var blogList = sessionStorage.getObject("sessionBlogList");
      console.log('no api call', blogList);
      vm.blogList = blogList; 
    }

  }



  function blogByIdCtrl(sessionStorage, blogDataById, $routeParams, $location, authentication) {

    var vm = this;

    vm.returnPage = $location.search() || '/money-finders-musings';

    vm.title = 'Blog Page';
    var blogid = $routeParams.blogid;
    vm.isLoggedIn = authentication.isLoggedIn();

    vm.blogList = sessionStorage.getObject('sessionBlogList');
    var all = sessionStorage.getObject('sessionAllBlogs');
    if(all){
      vm.blog = all.find(function(a) { return a._id == blogid; });
    }


    if(!all || !vm.blog){
      //if not found get it from server
      blogDataById
      .getBlog(blogid)
      .then((function (result) {
        vm.blog = result.data.blog;
        // console.log('here we go');

        //add the blog to session storage
        sessionStorage.addObject(vm.blog, 'sessionAllBlogs');

      }),(function (err) {
        console.log('error ',err.data.message);
      }));



    }
    
   

    vm.pressedDone = function() {
      $location.path('/money-finders-musings');
      // $location.url('/money-finders-musings');
      // window.location.href ='/money-finders-musings';
    }

    vm.deleteBlog = function(blogid, image_source) {
      if (confirm("Delete the post?")) {

        blogDataById.deleteBlog(blogid)
          .then((function(result) {
            sessionStorage.editObject(blogid, null);
            // console.log("all blogs after delete", sessionStorage.allBlogs);


            // window.location.href = '/money-finders-musings';
            $location.url('/money-finders-musings');

          }), (function(e) {
            console.log("cannot send from client side , deleting blog unsuccessful", e.statusText);
          }));
      } else {
        console.log('you pressed no');
      }
    };

    vm.editBlog = function() {
      $location.url('blog/' + blogid + '/edit');
    };

    vm.submitEdit = function() {

      vm.formError = '';
      // vm.blog =;
      if (!vm.blog || !vm.blog.title || !vm.blog.description || !vm.blog.content) {
        vm.formError = "All Fields are required, please try again.";
        return false;
      } else {
        data = {
          title: vm.blog.title,
          description: vm.blog.description,
          content: vm.blog.content,
          image_source: vm.blog.image_source,
          _id: vm.blog._id,
        };
        // console.log('-----', vm.blog.image_url);


        blogDataById.editBlog(data)
          .then((function(result) {
            console.log('success');
            //edit sotrage
            sessionStorage.editObject(vm.blog._id, data);
          }), (function(e) {
            console.log('error here');
            vm.formError = "Blog could not be saved. Please try again.";
          }));

      }

      $location.path('/blog/' + vm.blog._id);

      //goto url after reloadign page
      // window.location.href = '/blog/' + vm.data.blog._id;

    };
  }

  function modifyBlog(blog) {
    var rev = blog.reviews;
    var ret = [];
    var max_level = 4;

    for (var i = 0; i < rev.length; i++) {

      // ret.push(rev[i]);
      var count = rev[i].replies.length;
      // var rootCount = 0;
      // var maxLevel = 0;

      rev[i].replies.sort(function(a, b) {
        return (b.level - a.level);
      });
      // console.log(rev[i]);
      // console.log(ret);

      var j = 0;
      while (j < count) {
        if (rev[i].replies[j].level == 0) {
          break;
        } else {
          if (rev[i].replies[j].level == max_level && rev[i].replies[j].replies.length >= 2) {
            rev[i].replies[j].replies.sort(function(a, b) {
              return (a.level - b.level);
            });
          }


          var cur = rev[i].replies[j];
          // console.log("parent ",cur.parentid);

          var index = rev[i].replies.findIndex(function(a) {
            return a._id == cur.parentid;
          });
          // console.log('index ', index);
          rev[i].replies[index].replies.push(cur);
          rev[i].replies.splice(j, 1);
          j -= 1;

        }
        j += 1;

      }

      ret.push(rev[i]);

    }
    // console.log("here we have returning from modify data");
    blog.reviews = ret;
    return blog;
  }

  newBlogCtrl.$inject = ['addNewBlog', '$http', 'sessionStorage', '$location'];

  function newBlogCtrl(addNewBlog, $http, sessionStorage, $location) {
    var vm = this;
    vm.formData ={
      title: 'title',
      description: 'description',
      content: 'content'
    };

    vm.onSubmit = function() {

      vm.formError = '';
      if (!vm.formData || !vm.formData.title || !vm.formData.description || !vm.formData.content) {
        vm.formError = "All Fields are required, please try again.";
        return false;
      } else {
        var utc = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
        var timeutc = new Date().toJSON().slice(11,19);

        // console.log('content is ' , vm.formData.content);
        var data = {
          title: vm.formData.title,
          description: vm.formData.description,
          content: vm.formData.content,
          date: utc,
          time: timeutc,
          image_source: vm.image_source,
        };

        addNewBlog.addBlog(data)
          .then((function(result) {

            //adding new data to session storage
            var bList = {
              _id: result.data._id,
              title: result.data.title,
              description: result.data.description,
              image_source: data.image_source,
              date: result.data.date,
              time: result.data.time,
              reviews: [],
            };

            sessionStorage.addObject(bList, 'sessionBlogList');

            sessionStorage.addObject(bList,'sessionAllBlogs');
            // console.log("data here is ",data)
            $location.url('/money-finders-musings');

          }), (function(err) {
            console.log('error ', err);
          }));
      }

    };
  }


})();
