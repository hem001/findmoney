(function() {
  angular
    .module('findMoneyApp')
    .controller('registerCtrl', registerCtrl)
    .controller('loginCtrl', loginCtrl);

  registerCtrl.$inject = ['$location', 'authentication'];
  loginCtrl.$inject = ['$location', 'authentication'];

  function registerCtrl($location, authentication) {
    var vm = this;

    vm.title = "Registration Page.";

    vm.credentials = {
      name: '',
      email: '',
      password: ''
    };
    vm.returnPage = $location.search().page || '/';

    vm.onSubmit = function() {
      vm.formError = "";
      vm.isLoggedIn = authentication.isLoggedIn();
      // vm.isLoggedIn = true;
      if (!vm.credentials.name || !vm.credentials.email || !vm.credentials.password) {
        vm.formError = "All fields required, please try again";
        return false;
      } else {
        //Register

        vm.formError = '';
        if (vm.isLoggedIn) {
          authentication
            .register(vm.credentials)
            .then(function(result) {
              if (result.status === 200) {
                alert('Successfully registered a new user');

                $location.path(vm.returnPage);
              }else{

                vm.formError = 'oops something went wrong';
              }
            });
        }else{
          alert("You my friend are not logged in!!!");
        }

      }

    };


  }

  function loginCtrl($location, authentication) {
    var vm = this;
    this.title = "Sign in Page";

    vm.credentials = {
      email: '',
      password: ''
    };

    vm.returnPage = $location.search().page || '/';

    vm.onSubmit = function() {
      // console.log("hello");
      vm.formError = '';
      if (!vm.credentials.email || !vm.credentials.password) {
        vm.formError = "All fields required, please try again";
        return false;
      } else {
        vm.formError = "";
        authentication
          .login(vm.credentials)
          .then(function(result) {

            if (result.status === 200) {
              // console.log('here', result.status);
              alert('login successful');

              // $location.search('page', null);
              $location.path(vm.returnPage);
              // window.location.href= '/';
            }else{
              // alert('login unsuccessful');

              vm.formError = 'wrong username or password';
              // alert('wrong username and password');
            }
          });
          // ,( function(err) {
          //   console.log('error1');
          //   alert('Wrong');
          //   // vm.formError = err;
          // }));

      }
    };

  }
})();
