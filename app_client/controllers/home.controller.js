(function() {
  angular
    .module('findMoneyApp')
    .controller('homeCtrl', homeCtrl);


  homeCtrl.$inject = ['$uibModal', '$timeout', 'localStorage', 'sessionStorage', 'blogData', 'newsletterService'];

  function homeCtrl($uibModal, $timeout, localStorage, sessionStorage, blogData, newsletterService) {
    var vm = this;
    vm.title = 'Home Page';
    vm.tips = [{
      "title": "title1",
      "link": "/link1"
    }, {
      "title": "title2",
      "link": "/link2"
    }, {
      "title": "title3",
      "link": "/link3"

    }, {
      "title": "title4",
      "link": "/link4"
    }, {
      "title": "title5",
      "link": "/link5"
    }, {
      "title": "title6",
      "link": "/link6"
    }];

    //for carousel
    $('.carousel ').carousel();

    vm.showCookiesModal = function() {
      // console.log("hello");
      var modalInstance = $uibModal.open({
        animation: true,
        ariaLabelledBy: 'modal-header',
        ariaDescribedBy: 'modal-body',
        backdrop: 'static',
        keyboard: false,
        templateUrl: '/common/modal/cookies/cookies.modal.view.html',
        controller: 'cookiesModalCtrl',
        controllerAs: 'cookvm',
      });
      //required to view the modal
      modalInstance.result.then(function() {},
        function() {
          // console.log('review modal result');
        });
    };
    if (localStorage.getCookieStatus() != true) {
      $timeout(vm.showCookiesModal, 2000);

    }

    function clearFields() {
      vm.name = "";
      vm.email = "";
    }

    vm.deleteEmail= function () {
      console.log('delete email',vm.email);

      newsletterService.deleteEmail(vm.email)
      .then((function (result) {
          console.log('success');
        }),(function (e) {
          console.log('error deleting email ',e);
        }));
    };

    vm.getEmailList = function () {
      newsletterService
      .getEmailList()
      .then((function (result) {
        console.log('success', result);
      }),(function (err) {
        console.log('error getting email list',err);
      }));
    };

    vm.signupMailChimp = function () {
      newsletterService
      .signupMailChimp(vm.name, vm.email)
      .then((function (result) {
        console.log('success', result);
      }),(function (err) {
        console.log('error getting email list',err);
      }));
    };

    vm.sendEmail = function () {
      console.log('send email ',vm.email);
      
      newsletterService
      .sendEmail(vm.email)
      .then((function (result) {
        console.log('success');
      }),(function (e) {
        console.log('error sending newsletter ',e);
      }));

    }

    vm.storeNewsletterEmail = function () {
      vm.formError = "";
      if(!vm.name || !vm.email){
        vm.formError = "Please enter both fields.";
        return false;
      }
      else
      {

        console.log('Clicked', vm.email);
        
        newsletterService
        .storeEmail(vm.name,vm.email)
        .then((function (result) {
          console.log('success');
        }),(function (e) {
          console.log('error sending newsletter ',e);
        }));
        clearFields();

      }
      
    }

    // vm.signInForm = function() {
    //   var modalInstance = $uibModal.open({
    //     animation: true,
    //     ariaLabelledBy: 'modal-title',
    //     ariaDescribedBy: 'modal-body',
    //     templateUrl: '/common/modal/signIn/signIn.view.html',
    //     controller: 'signInCtrl',
    //     controllerAs: 'vm'

    //   });

    //   modalInstance.result.then(function() {},
    //     function() { console.log('modal result'); });

    // };


    //to get and set data in sessionStorage
    if (!sessionStorage.getObject('sessionBlogList')) {
      
        blogData.then((function(result) {
          vm.blogList = result.data.blogList;

          console.log('inside api call.bloglist');
          sessionStorage.setObject("sessionBlogList", vm.blogList);


        }), (function(e) {
          console.log(e.data.message);
          vm.errorMessage = e.data.message;
        }));
      } else {
        var blogList = sessionStorage.getObject("sessionBlogList");
        // console.log('no api call', blogList);

        vm.blogList = blogList; 
      }
    }



  


})();
