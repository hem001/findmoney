// review ctrl

(function() {
  angular
    .module("findMoneyApp")
    .controller('reviewCtrl', reviewCtrl);

  reviewCtrl.$inject = ['$uibModal', 'reviewService', '$routeParams', 'authentication', 'sessionStorage', '$window'];

  function reviewCtrl($uibModal, reviewService, $routeParams, authentication, sessionStorage, $window) {
    var revvm = this;
    revvm.isLoggedIn = authentication.isLoggedIn();
    var blogid = $routeParams.blogid;
    revvm.reviewButtonClicked = false;

    revvm.onReviewButonClick = function() {
      revvm.reviewButtonClicked = true;
    };

    revvm.cancelReview = function() {
      revvm.reviewButtonClicked = false;
    }

    revvm.onSubmit = function() {

      revvm.formError = "";
      if ((!revvm.reviewData || !revvm.reviewData.name || !revvm.reviewData.text) && !revvm.isLoggedIn) {
        console.log('trapped here');
        revvm.formError = "All fields are required, please try again.";
        return false;
      } else if (!revvm.isLoggedIn && revvm.reviewData.name.toLowerCase() === 'findmemoney') {
        revvm.formError = "That name is restricted";
        return false;

      } else {
        var utc = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
        var timeutc = (new Date()+"")
        timeutc = timeutc.slice(16,24);

        var img = revvm.isLoggedIn ? 'admin_pic.jpg' : 'user_pic.jpg';

        var data = {
          name: revvm.reviewData.name,
          text: revvm.reviewData.text,
          date: utc,
          time: timeutc,
          image_source: img,
        };
        // var blogid = $routeParams.blogid;
        if (revvm.isLoggedIn) {
          data.name = 'moneyfinder';
        }

        reviewService.addReview(data, blogid)
          .then((function(response) {
            // console.log('got back ', response.data);

            data._id = response.data._id;
            data.replies = response.data.replies;

            sessionStorage.addReview(blogid, data);
            revvm.reviewButtonClicked = false;
            revvm.reviewData.name = '';
            revvm.reviewData.text = '';

          }), (function(e) {
            console.log(e);
            revvm.formError = "could not save review";
          }));

      }

    };

  };


})();