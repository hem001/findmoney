(function(){
    angular
    .module("findMoneyApp")
    .controller('cookiesCtrl', cookiesCtrl);

    cookiesCtrl.$inject = ['$uibModal'];
    function cookiesCtrl($uibModal) {
    	var cookievm = this;

    	cookievm.onCookieClick = function () {
    		// console.log("hello");
    		var modalInstance = $uibModal.open({
    			animation: true,
    			ariaLabelledBy: 'modal-header',
    			ariaDescribedBy: 'modal-body',
    			templateUrl: '/common/modal/cookies/cookies.modal.view.html',
    			controller: 'cookiesModalCtrl',
    			controllerAs: 'cookvm',
    		});
    		//required to view the modal
    		modalInstance.result.then(function(){},
    		    function(){
    		        // console.log('review modal result');
    		    });
    	}
    }
}
)();