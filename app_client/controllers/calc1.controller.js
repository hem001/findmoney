(function () {
	angular
	.module('findMoneyApp')
	.controller('calc1Controller', calc1Controller);


	function calc1Controller() {
		var vm = this;
		vm.click=false;
		//to show legends
		vm.options ={legend: {display: true}};
		//to set all to single colour
		vm.datasetOverride =
        {
            label: 'principle',
			backgroundColor: "#803690",
            borderColor: "black"
        };

		vm.onClick = function(){
			
			var init_year = new Date().getFullYear();
			vm.labels=[];
			vm.data=[];
			var d=[];
			var a= 0;

			for(var i = 0; i<vm.t;i++){
				a = vm.p * (Math.pow((1+(vm.r/(vm.n*100))),(vm.n*(i+1))));
				vm.data.push(a);
				vm.labels.push(init_year+1+i);
			}
			vm.click = true;
		}

		

		  vm.series = ['Series A'];

		  
	}
})();
