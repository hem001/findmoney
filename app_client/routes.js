(function() {
  angular.module('findMoneyApp', ['ngRoute', 'ngStorage' ,'ui.bootstrap', 'ngSanitize','ngResource', 'chart.js', 'angular-google-adsense']);
  
  angular.module('findMoneyApp').config(['$routeProvider', '$locationProvider', config]);

  function config($routeProvider, $locationProvider) {


    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false
    });
    $routeProvider.when('/', {
        templateUrl: "/views/home.view.html",
        controller: 'homeCtrl',
        controllerAs: 'vm'
      })
      .when('/money-finders-musings', {
        templateUrl: '/views/all.blogs.view.html',
        controller: 'blogCtrl',
        controllerAs: 'vm',
      })
      .when('/search-page', {
        templateUrl: '/views/search-page.view.html',
        controller: 'searchPageCtrl',
        controllerAs: 'vm'
      })
      .when('/blog/:blogid', {
        templateUrl: '/views/blog.view.html',
        controller: 'blogByIdCtrl',
        controllerAs: 'vm'
      })
      .when('/blog/:blogid/new', {
        templateUrl: '/views/new.blog.view.html',
        controller: 'newBlogCtrl',
        controllerAs: 'vm'
      })
      .when('/blog/:blogid/edit', {
        templateUrl: '/views/edit.blog.view.html',
        controller: 'blogByIdCtrl',
        controllerAs: 'vm'
      })
      .when('/about', {
        templateUrl: '/views/about.view.html',
      })
      .when('/calculators', {
        templateUrl: '/views/calc1.view.html',
        controller: 'calc1Controller',
        controllerAs: 'vm',
      })
      .when('/register', {
        templateUrl: '/views/register.view.html',
        controller: 'registerCtrl',
        controllerAs: 'vm'
      })
      .when('/login', {
        templateUrl: '/views/login.view.html',
        controller: 'loginCtrl',
        controllerAs: 'vm'
      })
      .when('/tips/link1', {
        templateUrl: '/views/calc1.view.html',
        controller: 'calc1Controller',
        controllerAs: 'vm',

      })
      .when('/tips/link2', {
        templateUrl: '/views//link2.view.html',

      })
      .when('/tips/link3', {
        templateUrl: '/views//link3.view.html',

      })
      .when('/tips/link4', {
        templateUrl: '/views//link4.view.html',

      })
      .when('/tips/link5', {
        templateUrl: '/views//link5.view.html',

      })
      .when('/tips/link6', {
        templateUrl: '/views//link6.view.html',

      })
      .when('/disclaimer', {
        templateUrl: '/views//disclaimer.view.html',

      })
      .when('/privacy-policy', {
        templateUrl: '/views/privacy-policy.view.html',

      })
      .when('/contact-us', {
        templateUrl: '/views/contact-us.view.html',

      })
    // .otherwise({redirectTo: '/'})
    ;
  }

})();
