(function() {
  angular
    .module('findMoneyApp')
    .controller('signInCtrl', signInCtrl);


  signInCtrl.$inject = ['$uibModalInstance'];

  function signInCtrl($uibModalInstance) {
    var vm = this;
    vm.val = 1000;


    vm.modal = {
      cancel: function() {
        $uibModalInstance.dismiss('cancel');
      }
    };
  }
})();