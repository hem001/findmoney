(function () {
	angular
	.module('findMoneyApp')
	.controller('cookiesModalCtrl', cookiesModalCtrl);

	cookiesModalCtrl.$inject=['$uibModalInstance', '$window','localStorage'];
	function cookiesModalCtrl($uibModalInstance, $window, localStorage) {
		var cookvm = this;

		
		cookvm.onCancel = function () {
			// console.log("hello");
			localStorage.resetCookies();
			$window.location.href=('/');
		};

		cookvm.modal ={
			
			accept: function() {
				localStorage.setCookies();
			  	$uibModalInstance.dismiss('cancel');
			  	// $window.location.href="/";
			  	// $window.location.reload();
			}
		};
	}
})();