(function(){
  angular
  .module('findMoneyApp')
  .directive('blogCardDirective', blogCardDirective);

  function blogCardDirective(){
  return{
    restrict: 'EA',
    scope: {
        blog:'=blog'
    },
    templateUrl: '/common/directives/blog-cards/blogCards.template.html'
  };
}
})();