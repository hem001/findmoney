(function(){
    angular
    .module('findMoneyApp')
    .directive('replyDirective', replyDirective);

function replyDirective() {
    return{
        restrict: 'EA',
        scope:{
            reply:'=reply',
        },
        templateUrl: '/common/directives/reply/reply.directive.template.html',
        controller: 'replyDirectiveCtrl',
        controllerAs: 'repvm'
    };
}

})();