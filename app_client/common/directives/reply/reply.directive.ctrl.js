(function() {
  angular
    .module('findMoneyApp')
    .controller('replyDirectiveCtrl', replyDirectiveCtrl);

  replyDirectiveCtrl.$inject = ['$routeParams', 'replyService', 'authentication','$location', 'sessionStorage'];

  function replyDirectiveCtrl($routeParams, replyService, authentication, $location, sessionStorage) {
    var repvm = this;
    repvm.addReply = false;
    repvm.isLoggedIn = authentication.isLoggedIn();

    repvm.pic_url = $location.$$absUrl.replace($location.$$url, '') +'/uploads';
    // console.log(repvm.pic_url);

    repvm.onReplyClick = function() {
      // console.log('reply clicked');
      repvm.addReply = true;
    };

    repvm.cancelReply = function() {
      repvm.addReply = false;
      // console.log('cancelled');
    };

    repvm.postReply = function(replyid, reviewid, level, rep_parentid) {

      repvm.formError = "";
      if ((!repvm.replyData || !repvm.replyData.name || !repvm.replyData.text) && !repvm.isLoggedIn ) {
        repvm.formError = "All fields are required, please try again";
        return false;
      }else if(!repvm.isLoggedIn && repvm.replyData.name.toLowerCase() === 'findmemoney' ) {
        repvm.formError = "That name is restricted";
        return false;
      } else {
        var utc = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
        var timeutc = (new Date()+"")
        timeutc = timeutc.slice(16,24);
        
        var img = repvm.isLoggedIn ? 'admin_pic.jpg' : 'user_pic.jpg';
        

        var data = {
          name: repvm.replyData.name,
          text: repvm.replyData.text,
          date: utc,
          time: timeutc,
          level: level,
          reviewid: reviewid,
          image_source: img,
          // parentid: replyid,
        };
        if (repvm.isLoggedIn){
          data.name = 'moneyfinder';
        }
        if(level >5){
          data.parentid = rep_parentid;
        }else{
          data.parentid = replyid;
        }
        var blogid = $routeParams.blogid;

        replyService.addReply(data, blogid, reviewid)
          .then((function(response) {
            // console.log('reply back success', response.data);

            //to hide the form
            repvm.addReply = false;
            repvm.replyData.name = '';
            repvm.replyData.text = '';

            data._id = response.data._id;
            data.replies = response.data.replies;

            sessionStorage.addReply($routeParams.blogid, reviewid, data);


            // $location.path('/blog/' + blogid);
            // console.log("response is", response.data);

          }), (function(e) {
            console.log(e);
            repvm.formError = "could not save reply";
          }));
      }

    };

    repvm.onDeleteClick = function(reviewid, replyid) {
      // console.log('to delete id ', reviewid,replyid );

      if (confirm("Delete the post") && authentication.isLoggedIn()) {
        ids = {
          reviewid: reviewid,
          replyid: replyid,
          blogid: $routeParams.blogid,
        };
        // sessionStorage.deleteReply(ids.blogid, ids.reviewid, ids.replyid);

        replyService.removeReply(ids)
          .then((function(data) {
            sessionStorage.deleteReply(ids.blogid, ids.reviewid, ids.replyid);

            // console.log('reply deleted succcessfully', data);
            $location.path('/blog/' + blogid);

          }), (function(err) {
            // console.log(err);
            alert( "could not delete reply");
          }));
      }

    };

  }
})();