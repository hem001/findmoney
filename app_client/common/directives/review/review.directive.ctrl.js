(function() {
  angular
    .module('findMoneyApp')
    .controller('reviewDirectiveCtrl', reviewDirectiveCtrl);

  reviewDirectiveCtrl.$inject = ['$routeParams', 'reviewService','replyService', 'authentication','sessionStorage','$location'];

  function reviewDirectiveCtrl($routeParams, reviewService, replyService, authentication, sessionStorage, $location) {
    var vm = this;
    vm.pic_url = $location.$$absUrl.replace($location.$$url, '') +'/uploads';
    blogid = $routeParams.blogid;
    vm.isLoggedIn = authentication.isLoggedIn();


    vm.onDeleteClick = function(reviewid) {

      if (confirm("Do you want to delete the review?")) {
        reviewService
          .removeReview(blogid, reviewid)
          .then((function(result) {
            sessionStorage.deleteReview(blogid, reviewid);

            $location.path( '/blog/' + blogid);


          }), (function(err) {
            console.log(err);
          }));
      } else {
        console.log('pressed cancel');
      }

    };
    
  }


})();