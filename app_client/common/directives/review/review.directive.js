
(function(){
    angular
    .module('findMoneyApp')
    .directive('reviewDirective', reviewDirective);

function reviewDirective() {
    return{
        restrict: 'EA',
        scope:{
            review:'=review',
        },
        templateUrl: '/common/directives/review/review.template.html',
        controller: 'reviewDirectiveCtrl',
        controllerAs: 'vm'
    };
}

})();