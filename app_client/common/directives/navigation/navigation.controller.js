(function(){
   angular
   .module('findMoneyApp')
   .controller('navigationCtrl', navigationCtrl)
   ;


   navigationCtrl.$inject = ['$location', 'authentication', 'localStorage'];

   function navigationCtrl($location, authentication, localStorage){
    var vm = this;
    vm.currentPath = $location.path();
    vm.hasCookies = localStorage.getCookieStatus();
    vm.paths = (vm.currentPath==='/privacy-policy' || vm.currentPath==='/disclaimer' || vm.currentPath==='/contact-us')
    vm.hideNavbar = !(vm.paths && (!vm.hasCookies));
    
    // console.log('\\',vm.hideNavbar, vm.paths, vm.hasCookies);

    vm.isLoggedIn = authentication.isLoggedIn();
    vm.currentUser = authentication.currentUser();

    vm.logout = function(){
        authentication.logout();
        vm.isLoggedIn = authentication.isLoggedIn();
        $location.path('/');
    };

   }

})();
