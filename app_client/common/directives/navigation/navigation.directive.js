(function(){
  angular
  .module('findMoneyApp')
  .directive('navigationDirective', navigationDirective);

function navigationDirective(){
  return{
    restrict: 'EA',
    templateUrl: '/common/directives/navigation/navigation.template.html',
    controller: 'navigationCtrl as vm'
  };
}
})();