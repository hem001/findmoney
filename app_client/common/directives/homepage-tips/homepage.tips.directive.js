(function(){
  angular
  .module('findMoneyApp')
  .directive('homepageTipsDirective', homepageTipsDirective);

function homepageTipsDirective(){
  return{
    restrict: 'EA',
    scope: {
        tip:'=tip'
    },
    templateUrl: '/common/directives/homepage-tips/homepage.tips.template.html'
  };
}
})();