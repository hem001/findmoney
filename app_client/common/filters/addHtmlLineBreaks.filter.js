(function () {
  
  angular
    .module('findMoneyApp')
    .filter('addHtmlLineBreaks', addHtmlLineBreaks);

  function addHtmlLineBreaks () {
    return function (text) {
      // var output = text.replace(/\n/g , '<br/>');
      var t = ""+text;
      var output = t.replace(/\n/g, "<br/>");
      return output;
    };
  }

})();