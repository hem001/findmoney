(function() {
  angular
    .module('findMoneyApp')
    .factory('sessionStorage', sessionStorage);

  sessionStorage.$inject = ['$sessionStorage'];

  function sessionStorage($sessionStorage) {
    var storage = {};
    // var all = $sessionStorage['sessionAllBlogs'];

    storage.clear = function() {
      $sessionStorage.$reset();
    }

    storage.setObject = function(key, value) {
      if(key === 'sessionBlogList'){
        $sessionStorage[key] = value;
        return;
      }
      if(key === 'sessionAllBlogs'){
        var all = [];
        for (var i = 0; i < value.length; i++) {
          all.push(modifyReviews(value[i]));
        }
        $sessionStorage[key] = all;
      }
      
    };

    storage.getObject = function(key) {
      // console.log('print all list',key, $sessionStorage[key])
      return $sessionStorage[key];
    };

    storage.editObject = function(id, data) {
      //need to edit both lists

      var all = $sessionStorage['sessionAllBlogs'];
      var allList = $sessionStorage['sessionBlogList'];

      var index = findPosition(all, id);
      var index1 = findPosition(allList, id);

      if (data) {
        //this is for sessionAllBlog
        all[index].title = data.title;
        all[index].description = data.description;
        all[index].text = data.text;
        all[index].image_source = data.image_source;

        // //this is for sessionBlogList
        allList[index1].title = data.title;
        allList[index1].description = data.description;
        allList[index1].text = data.text;
        allList[index1].image_source = data.image_source;

      } else {
        all.splice(index, 1);
        allList.splice(index1,1);
      }
      $sessionStorage.sessionAllBlogs = all;
      $sessionStorage.sessionBlogList = allList;
    };

    storage.addObject = function(data, key) {
      var all = $sessionStorage[key];


      if(!all){
        all = [];
      }
      
      // console.log('all is ', all);
      all.push(data);

      $sessionStorage[key] = all;

    };

    storage.addReview = function(blogid, review) {
      var all = $sessionStorage['sessionAllBlogs'];
      var index = findPosition(all, blogid);

      all[index].reviews.push(review);

      $sessionStorage.sessionAllBlogs = all;
    };

    storage.deleteReview = function(blogid, reviewid) {
      var all = $sessionStorage['sessionAllBlogs'];
      var index = findPosition(all, blogid);
      var rev_index = findPosition(all[index].reviews, reviewid);

      all[index].reviews.splice(rev_index, 1);

      $sessionStorage.sessionAllBlogs = all;
    };

    //add replies
    storage.addReply = function(blogid, reviewid, reply) {
      var all = $sessionStorage['sessionAllBlogs'];
      // console.log("adding reply ", reply);
      var index = findPosition(all, blogid);
      var rev_index = findPosition(all[index].reviews, reviewid);
      var review = all[index].reviews[rev_index];
      // console.log('all ', review.replies);

//disassemble the replies and push reply, then modify replies
      var dis = disassemble(review.replies, []);
      dis.push(reply);
      dis = modifyReplies(dis);

      review.replies = dis;
      // console.log('blog before ', all[index].reviews);
      // var blog = modifyReviews(all[index]);
      // all[index] = blog;

      // console.log(' blog is ',all[index]);
      $sessionStorage.sessionAllBlogs = all;

    };
    // delete replies

    storage.deleteReply = function (blogid, reviewid, replyid) {
      var all = $sessionStorage['sessionAllBlogs'];
      var index = findPosition(all, blogid);
      var rev_index = findPosition(all[index].reviews, reviewid);
      var review = all[index].reviews[rev_index];

      // disassemble replies , delete and reassemble them
      var disList = disassemble(review.replies, []);
      rep_index = findPosition(disList, replyid);
      disList.splice(rep_index,1);
      disList = modifyReplies(disList);
      review.replies = disList;
      
      $sessionStorage.sessionAllBlogs = all;
    };

    return storage;

  }

  function findPosition(list, id) {
    var index = list.findIndex(function(a) {
      return (a._id == id);
    });
    return index;
  }


  function modifyReviews(blog) {
    var rev = blog.reviews;
    var ret = [];
    var max_level = 5;

    for (var i = 0; i < rev.length; i++) {

      //here -----------------

      rev[i].replies = modifyReplies(rev[i].replies);
      ret.push(rev[i]);
    }
    // console.log("here we have returning  ---------a");
    blog.reviews = ret;
    return blog;
  }


  function modifyReplies(replies) {
    if (replies.length < 2) return replies;

    // this is set at reply.directive.controller
    max_level = 5;

    //sort replies in decending order of their level
    replies.sort(function(a, b) {
      return (b.level - a.level);
    });

    //put replies in their parents replies list starting at the deepest 
    var j = 0;
    //must guarantee that loop never runs infinitely
    while (1) {
      if (replies[0].level == 0) {
        break;
      } else {
        // put the reply to its parent reply
        var cur = replies[0];
        var parent = replies.findIndex(function(a) {
          return a._id == cur.parentid;
        });

        replies[parent].replies.push(cur);
        if (replies[parent].level == 5 && replies[parent].replies.length >= 2) {
          replies[parent].replies.sort(function(a, b) {
            return (a.level - b.level);
          });
        }

        //remove current from the list
        replies.splice(0, 1);
      }
    }
    return replies;
  }

  function disassemble(replies, retList){
  if (!replies){
    console.log('nothing');
    return retList;
  }
  for(var i = 0; i < replies.length; i++){
    disassemble(replies[i].replies, retList);
    replies[i].replies = [];
    retList.push(replies[i]);
  }
  return retList;
  }

})();