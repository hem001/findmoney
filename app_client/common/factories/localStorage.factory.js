(function() {
  angular
    .module('findMoneyApp')
    .factory('localStorage', localStorage);

  localStorage.$inject = ['$localStorage'];

  function localStorage($localStorage) {
    var storage = {};

    storage.clear = function() {
      $localStorage.$reset();
    };

    storage.setCookies = function() {
      $localStorage['cookies'] = true;
    };

    storage.resetCookies = function() {
      $localStorage['cookies'] = false;
    };
    storage.getCookieStatus = function() {
      if(!$localStorage['cookies']){
        return false;
      }
      // return false;
      return $localStorage['cookies'];
    };
    return storage;
  }

})();