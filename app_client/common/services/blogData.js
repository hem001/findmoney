(function() {
    angular
      .module('findMoneyApp')
      .service('blogData', blogData)
      .service('blogDataById', blogDataById)
      .service('addNewBlog', addNewBlog)
      .service('reviewService', reviewService)
      .service('replyService', replyService)
      .service('newsletterService', newsletterService);

    blogData.$inject = ['$http'];

    function blogData($http) {
      return $http.get('/api/allBlogs/');
    }

    blogDataById.$inject = ['$http', 'authentication'];

    function blogDataById($http, authentication) {

      var getBlog = function(id) {
        return $http.get('/api/blog/' + id);
      };

      var deleteBlog = function(id) {
        // console.log('delete blog ', authentication.getToken());
        return $http.delete('/api/blog/' + id, {
          headers: {
            Authorization: 'Bearer ' + authentication.getToken()
          }
        });
      };

      var editBlog = function(data) {
        // console.log("help");
        return $http.put('/api/blog/edit', data, {
          headers: {
            Authorization: 'Bearer ' + authentication.getToken()
          }
        });
      };

      var deletePic = function(image_source) {
        // console.log('filename is ',image_source);
        return $http.delete('/deleteImage', { params: { "image_source": image_source } });
      };


      return {
        getBlog: getBlog,
        deleteBlog: deleteBlog,
        editBlog: editBlog,
        deletePic: deletePic,
      };
    }


    addNewBlog.$inject = ['$http', 'authentication'];

    function addNewBlog($http, authentication) {

      var addBlog = function(data) {
        return $http.post('/api/blog/new', data, {
          headers: {
            Authorization: 'Bearer ' + authentication.getToken()
          }
        });
      };

      var postPic = function(fd) {

        return $http.post(
          '/uploadImages',
          fd, {
            transformRequest: angular.identity,
            headers: {
              'Content-Type': undefined
            }

          });
      };

      return {
        addBlog: addBlog,
        postPic: postPic,
      };

    }

    reviewService.$inject = ['$http', 'authentication'];

    function reviewService($http, authentication) {

      var addReview = function(data, blogid) {
        return $http.post('/api/blog/' + blogid + '/review', data);
      };

      var removeReview = function(blogid, reviewid) {

        var url = '/api/blog/' + blogid + '/review/' + reviewid;
        return $http.delete(url, {
          headers: {
            Authorization: 'Bearer ' + authentication.getToken()
          }
        });
      };

      return {
        addReview: addReview,
        removeReview: removeReview,
      };
    }



    replyService.$inject = ['$http', "authentication"];

    function replyService($http, authentication) {


      var addReply = function(data, blogid, reviewid) {
        // console.log(data, blogid, reviewid);
        return $http.post('/api/blog/' + blogid + '/review/' + reviewid + '/new', data);
      };

      var removeReply = function(ids) {
        var url = '/api/blog/' + ids.blogid + '/review/' + ids.reviewid + '/reply/' + ids.replyid;
        return $http.delete(url);

      };


      return {
        addReply: addReply,
        removeReply: removeReply,
      };
    }

    newsletterService.$inject = ['$http'];

    function newsletterService($http) {

      var storeEmail = function(name, email) {
        return $http.post('/api/storeEmail',{params: {name: name, email: email}});
      };
      var deleteEmail = function (email) {
        return $http.delete('/api/deleteEmail', { params: {email: email } });
        
      };

      var getEmailList = function () {
        return $http.get('/api/getEmailList');
      };

      var sendEmail = function (email) {

        return $http.post('/api/sendEmail', {params: {name: name, email: email}});
      };

      var signupMailChimp = function (name, email) {  
        console.log('here we go');
        return $http.post('/api/signupMailChimp', {params: {name: name, email: email}});
      };

        return {
          storeEmail: storeEmail,
          deleteEmail: deleteEmail,
          getEmailList:getEmailList,
          sendEmail: sendEmail,
          signupMailChimp: signupMailChimp,
        };
      }

    })();