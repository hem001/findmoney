(function() {
  angular
    .module('findMoneyApp')
    .service('authentication', authentication);

  authentication.$inject = ['$window', '$http'];

  function authentication($window, $http) {

    var saveToken = function(token) {
      $window.localStorage['findMoney-token'] = token;
      // console.log("token in saveToken is ", $window.localStorage['findMoney-token']);
    };

    var getToken = function() {
      return $window.localStorage['findMoney-token'];
    };

    var register = function(user) {
      // console.log('register service');

      return $http.post('/api/register', user)
      .then(function(res) {
        // console.log("inside auth service/register token received", res.data.token);

        saveToken(res.data.token);
        return res;

      }, function(e) {
        // console.log("error is ", e);
        return e;
      });
    };

    var login = function(user) {
      // console.log("-------------------");
      return $http.post('/api/login', user)
      .then((function(res) {
        // console.log("inside auth service/login");

        saveToken(res.data.token);
        return res;
      }),(function(e) { 
        // console.log("error ",e);
        return e; 
      }
        ));
    };

    var logout = function() {
      $window.localStorage.removeItem('findMoney-token');
    };

    var isLoggedIn = function() {
      var token = getToken();

      if (token) {
        var payload = JSON.parse($window.atob(token.split('.')[1]));

        return payload.exp > Date.now() / 1000;
      } else {
        return false;
      }
    };

    var currentUser = function() {
      if (isLoggedIn()) {
        var token = getToken();
        var payload = JSON.parse($window.atob(token.split('.')[1]));
        return {
          email: payload.email,
          name: payload.name
        };
      }
    };





    return {
      saveToken: saveToken,
      getToken: getToken,
      register: register,
      login: login,
      logout: logout,
      isLoggedIn: isLoggedIn,
      currentUser: currentUser

    };
  }
})();