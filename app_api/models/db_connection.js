var mongoose = require('mongoose');
var gracefulShutdown;
var dbURI;

if (process.env.NODE_ENV === 'production'){
    dbURI = process.env.MONGOLAB_URI;

} else{
    // dbURI = 'mongodb://localhost:27017/findmoney';
    dbURI = 'mongodb://hem098:Satchboogi3@ds121636.mlab.com:21636/findmoney';
}


mongoose.connect(dbURI);
mongoose.connection.on('connected', () => {
    console.log('mongoose connected to ' + dbURI);
});
mongoose.connection.on('error', (error) => {
    console.log(' Error ' + error);
});
mongoose.connection.on('disconnected', () => {
    console.log('mongoose disconnected  ');
});
gracefulShutdown = (msg, callback) => {
    mongoose.connection.close(() => {
        console.log('Mongoose disconnected through ' + msg);
        callback();
    });
};
//For nodemon restarts
process.once('SIGUSR2', () => {
    gracefulShutdown('nodemon restart', () => {
        process.kill(process.pid, 'SIGUSR2');
    });
});
//For app terminstaion
process.on('SIGINT', () => {
    gracefulShutdown('app termination', () => {
        process.exit(0);
    });
});
require('./db_schemas');