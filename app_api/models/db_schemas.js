var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');


var userSchema = new mongoose.Schema({
  email: {
    type: String,
    // unique: true,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  hash: String,
  salt: String
  // image_source: String
});

userSchema.methods.setPassword = function(password) {

  this.salt = crypto.randomBytes(16).toString('hex');
  this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
  // console.log('enter ',this.salt,'            ',this.hash)
};

userSchema.methods.validatePassword = function(password) {
  console.log('validating password');
  var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
  return this.hash === hash;
};

userSchema.methods.generateJwt = function() {
  var expiry = new Date();
  expiry.setDate(expiry.getDate() + 7);


  return jwt.sign({
    _id: this._id,
    email: this.email,
    name: this.name,
    exp: parseInt(expiry.getTime() / 1000),
  }, process.env.JWT_SECRET);
};

var replySchema = new mongoose.Schema({
  name:{
    type: "String",
    required: true
  },
  text: String,
  date: String,
  time: String,
  level: Number,
  reviewid: String,
  parentid: String,
  childrenids:[String],
  image_source: String,
});
replySchema.add({
  replies: [replySchema],
  
});

var reviewSchema = new mongoose.Schema({
  name: {
    type: "String",
    required: true
  },
  text: String,
  date: String,
  time: String,
  replies: [replySchema],
  image_source: String,

});

var blogsSchema = new mongoose.Schema({
  _id: String,
  title: String,
  description: String,
  content: String,
  date: String,  
  time: String,
  reviews: [reviewSchema],
  image_source: String,
});

var blogListSchema = new mongoose.Schema({
  _id: String,
  title: String,
  description: String,
  date: String,
  time: String,
  image_source: String,
});

var emailListSchema = new mongoose.Schema({
  _id: String,
  name: String,
  email: String,
});


mongoose.model('User', userSchema, 'user');
mongoose.model('Blogs', blogsSchema, 'blogs');
mongoose.model('BlogList', blogListSchema, 'blogList');
mongoose.model('EmailList', emailListSchema, 'emailList');
