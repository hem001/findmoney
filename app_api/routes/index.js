var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');
var auth = jwt({
    secret: process.env.JWT_SECRET,
    userProperty: 'payload'
});

var ctrlBlog = require('../controllers/blog');
var ctrlContacts = require('../controllers/contacts');
var ctrlAuth = require('../controllers/authentication');
var ctrlReview = require('../controllers/review');
var ctrlReply = require('../controllers/reply');
// var ctrlSearch = require('../controllers/search');
var ctrlHome = require('../controllers/home');

router.get('/allBlogs', ctrlBlog.getBlogList);
router.get('/blog/:blogid', ctrlBlog.getBlogById);

// router.get('/blog/new', ctrlBlog.newBlogPage);
router.post('/blog/new', auth, ctrlBlog.uploadNewBlog);
router.delete('/blog/:blogid', auth, ctrlBlog.deleteBlogById);
router.put('/blog/edit', auth, ctrlBlog.editBlogById);

router.post('/contacts', ctrlContacts.addContact);

//user authentication
router.post('/register', ctrlAuth.register);
router.post('/login', ctrlAuth.login);

//routes for reviews
router.post('/blog/:blogid/review/', ctrlReview.createReview);
router.delete('/blog/:blogid/review/:reviewid', auth, ctrlReview.deleteReviewById);

// router.get('/blog/:blogid/review/:reviewid', ctrlReview.getReviewById);
// router.put('/blog/:blogid/review/:reviewid', ctrlReview.editReviewById);

//reply routes
router.post('/blog/:blogid/review/:reviewid/new', ctrlReply.createReply);
router.delete('/blog/:blogid/review/:reviewid/reply/:replyid', ctrlReply.deleteReplyById);

// router.get('/search-page', crtlSearch.search);

//newsletter routes
router.post('/sendEmail', ctrlHome.sendEmail);

router.post('/storeEmail', ctrlHome.storeEmail);
router.delete('/deleteEmail', ctrlHome.deleteEmail);
router.get('/getEmailList', ctrlHome.getEmailList);

router.post('/signupMailChimp', ctrlHome.signupMailChimp);

module.exports = router;
