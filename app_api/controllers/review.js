var mongoose = require('mongoose');
var Blog = mongoose.model('Blogs');
var User = mongoose.model('User');
// var authFunc = require('./blog');

var sendJsonResponse = function(res, status, content) {
  res.status(status);
  res.json(content);
};

var getAuthor = function(req, res, callback) {
  // console.log('inside get author');
  if (req.payload && req.payload.email) {

    User
      .findOne({ email: req.payload.email })
      .exec(function(err, user) {
        if (!user) {
          sendJsonResponse(res, 404, {
            "message": "User not found"
          });
          return;
        } else if (err) {
          console.log(err);
          sendJsonResponse(res, 404, err);
          return;
        }
        callback(req, res, user.name);
      });
  } else {
    sendJsonResponse(res, 404, {
      "message": "User not found"
    });
    return;
  }
};

module.exports.createReview = function(req, res) {
  // console.log("inside review ", req.body.name);

  if (req.params.blogid) {
    Blog
      .findById(req.params.blogid)
      .select('reviews')
      .exec(
        function(err, blog) {
          if (err) {
            sendJsonResponse(res, 400, err);
          } else {
            //body
            blog.reviews.push({
              name: req.body.name,
              text: req.body.text,
              date: req.body.date,
              time: req.body.time,
              image_source: req.body.image_source,
            });
            blog.save(function(err, blog) {
              var thisReview;
              if (err) {
                sendJsonResponse(res, 400, err);
              } else {
                thisReview = blog.reviews[blog.reviews.length - 1];
                sendJsonResponse(res, 201, thisReview);
              }
            });

          }
        });
  } else {
    sendJsonResponse(res, 404, {
      "message": "Not found, blogid required"
    });
  }
};

module.exports.deleteReviewById = function(req, res) {

  getAuthor(req, res, function(req, res, userName) {

    var blogid = req.params.blogid;
    var reviewid = req.params.reviewid;

    if (!blogid || !reviewid) {
      sendJsonResponse(res, 404, {
        "message": "Not found, blogid and reviewid are both required"
      });
      return;
    }

    Blog.findById(blogid)
      .select('reviews')
      .exec(
        function(err, blog) {
          if (!blog) {
            sendJsonResponse(res, 400, {
              "message": "Blog id not found"
            });
          } else if (err) {
            sendJsonResponse(res, 400, err);
            return;
          }
          if (blog.reviews && blog.reviews.length > 0) {
            if (!blog.reviews.id(req.params.reviewid)) {
              sendJsonResponse(res, 404, {
                "message": "reviewid not found"
              });
            } else {
              blog.reviews.id(reviewid).remove();
              blog.save(function(err) {
                if (err) {
                  sendJsonResponse(res, 404, err);
                } else {
                  sendJsonResponse(res, 204, null);
                }
              });
            }
          } else {
            sendJsonResponse(res, 404, {
              "message": " No review to delete"
            });
          }
        });
  });

};