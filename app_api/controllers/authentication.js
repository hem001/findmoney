var passport = require('passport');
var mongoose = require('mongoose');
var User = mongoose.model('User');


var sendJSONresponse = function(res, status, content) {
  res.status(status);
  res.json(content);
};


module.exports.register = function(req, res){

    if(!req.body.name || !req.body.email || !req.body.password ){
        sendJSONresponse(res, 400, {
            "message": "All fields required"
        });
        return;
    }

    var user = new User();

    user.name = req.body.name;
    user.email = req.body.email;
    user.setPassword(req.body.password);

    user.save(function(err, data){
        var token;

        if(err){
            // console.log('--------------------------------3');

            console.log('error in register',err);
            sendJSONresponse(res, 404, err);
        }else{

            token = user.generateJwt();
            sendJSONresponse(res, 200, {
                "token": token
            });
        }
    });

};

module.exports.login = function(req, res){
    

    if(!req.body.email || !req.body.password){
        sendJSONresponse(res, 400, {
            "message": "All fields required"
        });
        return;
    }

    passport.authenticate('local', function(err, user, info){
        // console.log('-------------------1');
        var token;
        if(err){
            sendJSONresponse(res, 404, err);
        }
        if(user){
            token = user.generateJwt();
            sendJSONresponse(res, 200, {
                "token": token
            });
        }else{
            sendJSONresponse(res, 401, info);
        }

    })(req, res); //make sure req and res are available to Passport
};







