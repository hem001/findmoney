var mongoose = require('mongoose');
var ObjectId = require('mongodb').ObjectId;

var Blog = mongoose.model('Blogs');
var User = mongoose.model('User');
var BlogList = mongoose.model('BlogList');


var sendJsonResponse = function(res, status, content) {
  res.status(status);
  res.json(content);
};

var getAuthor = function(req, res, callback) {
  // console.log('inside get author');

  if (req.payload && req.payload.email) {

    User
      .findOne({ email: req.payload.email })
      .exec(function(err, user) {
        if (!user) {
          sendJsonResponse(res, 404, {
            "message": "User not found"
          });
          return;
        } else if (err) {
          // console.log(err);
          sendJsonResponse(res, 404, err);
          return;
        }
        callback(req, res, user.name);
      });
  } else {
    sendJsonResponse(res, 404, {
      "message": "User not found"
    });
    return;
  }
};



//edit blog
module.exports.editBlogById = function(req, res) {
  

  getAuthor(req, res, function(req, res, userName) {
    // console.log("byid edit", req.body._id);


    if (req.body._id) {

      Blog
        .findById(req.body._id)
        .exec(
          function(err, blog) {
            blog.title = req.body.title;
            blog.description = req.body.description;
            blog.content = req.body.content;
            blog.image_source = req.body.image_source;
            blog.save(function(err, blog) {
              //console.log("---------->", req.body.image_source);
              if (err) {
                //console.log("byid edit", err);

                sendJsonResponse(res, 404, err);
              } else {
                //console.log("byid edit done",blog.image_source);

                sendJsonResponse(res, 201, blog);
              }
            });
          });

        //edit bloglist
        BlogList
          .findById(req.body._id)
          .exec(
            function(err, blog) {
              blog.title = req.body.title;
              blog.description = req.body.description;
			  blog.image_source = req.body.image_source;
              blog.save(function(err, blog) {

                if (err) {
                  console.log("byid edit", err);

                  sendJsonResponse(res, 404, err);
                } else {
                  console.log("blog id edit done");

                  // sendJsonResponse(res, 201, blog);
                }
              });
            });
    }
  });
};




//upload new blog to database
module.exports.uploadNewBlog = function(req, res) {
  getAuthor(req, res, function(req, res, userName) {

    // console.log('username is ', userName);
    var id = ObjectId();
    if (userName) {
      Blog
        .create({
          // author: userName,
          _id: id,
          title: req.body.title,
          description: req.body.description,
          content: req.body.content,
          date: req.body.date,
          time: req.body.time,
          image_source: req.body.image_source,
        }, function(err, blog) {
          if (err) {
            sendJsonResponse(res, 404, { "message": 'Error on adding blog to db' });
          } else {
            // sendJsonResponse(res, 200, blog);
          }
        });
      //add to bloglist
      BlogList
        .create({
          // author: userName,
          _id: id,
          title: req.body.title,
          description: req.body.description,
          date: req.body.date,
          time: req.body.time,
          image_source: req.body.image_source,
        }, function(err, blog) {
          if (err) {
            sendJsonResponse(res, 404, { "message": 'Error on adding blog to dbList' });
          } else {
            // console.log(blog);
            sendJsonResponse(res, 200, blog);
          }
        });
    }
  });
};

module.exports.deleteBlogById = function(req, res) {
  var blogid = req.params.blogid;

  getAuthor(req, res, function(req, res, userName) {
    if (blogid) {
      Blog
        .findByIdAndRemove(blogid)
        .exec(
          function(err, blog) {
            if (err) {

              // console.log(err);
              sendJsonResponse(res, 404, err);
              return;
            }

            // console.log('blog deleted successfully');
            // sendJsonResponse(res, 204, null);
          });

      //remove from bloglist
      BlogList.findByIdAndRemove(blogid)
        .exec(
          function(err, blog) {
            if (err) {

              sendJsonResponse(res, 404, err);
              return;
            }

            // console.log('blog deleted successfully');
            sendJsonResponse(res, 204, null);
          });
    }
  });
};

module.exports.getBlogList = function(req, res) {
  //module to get the list of blogs
  BlogList
    .find({})
    .exec( function(err, data) {
      if (!data.length) {
        console.log('no data in db found');
        sendJsonResponse(res, 400, { "message": "empty database: no blogs to display" });
      } else if(err){
        console.log('no db found',err);

        sendJsonResponse(res,400, {"message": "error fetching from database"});
      }else {
        console.log('data in db found',data);

        sendJsonResponse(res, 200, {
          "blogList": data
        });
      }
    });
};

module.exports.getBlogById = function(req, res) {
  // console.log('blog id in api is '+req.params.blogid);

  Blog
    .findById(req.params.blogid)
    .exec(function(err, blog) {
      if (!blog) {
        sendJsonResponse(res, 404, { "message": "Blog id not found" });
        return;
      } else if (err) {
        sendJsonResponse(res, 404, err);
        return;
      } else {
        sendJsonResponse(res, 200, {
          "blog": blog,
        });
      }
    });

};
