var mongoose = require('mongoose');
var nodemailer = require('nodemailer');
var EmailList = mongoose.model('EmailList');
var ObjectId = require('mongodb').ObjectId;
var createCsvWriter = require('csv-writer').createObjectCsvWriter;
var path = "emailList.csv";
var request = require('request');

var csvWriter = createCsvWriter({
  path: path,
  header: [
    // {id:'_id', title:'Id'},
    { id: 'name', title: 'Name' },
    { id: 'email', title: 'Email' },
  ]
});

var fs = require('fs');

var sendJsonResponse = function(res, status, content) {
  res.status(status);
  res.json(content);
};


module.exports.storeEmail = function(req, res) {
  if (req.body.params.name && req.body.params.email) {
    EmailList
      .create({
        _id: new ObjectId(),
        name: req.body.params.name,
        email: req.body.params.email,
      }, function(err, email) {
        if (err) {
          sendJsonResponse(res, 404, { "message": 'Error in saving email' });
        } else {
          sendJsonResponse(res, 200, email);
        }
      });
  } else {
    sendJsonResponse(res, 400, null);
  }


};
module.exports.getEmailList = function(req, res) {
  EmailList
    .find({}, function(err, data) {
      if (err) {
        console.log('error', err);
        sendJsonResponse(res, 404, 'No result found!');
      } else {
        // console.log('success');
        try {
          fs.unlinkSync(path);
        } catch (err) {
          console.log('error ', err);
        }

        csvWriter
          .writeRecords(data)
          .then((function() {
            console.log('successfully written csv file', );
            sendJsonResponse(res, 200, data);
          }), (function(err) {
            console.log('error', err);
            sendJsonResponse(res, 404, 'Error getting the email list');
          }))

      }
    });

}

module.exports.deleteEmail = function(req, res) {
  var emailid = req.query.email;

  if (req.query.email) {
    EmailList
      .findOneAndRemove({ "email": emailid },
        function(err, ret) {
          if (err) {
            console.log('not found');
            sendJsonResponse(res, 404, err)
          } else {
            if (ret) {
              sendJsonResponse(res, 200, "Deleted Successfully");
            } else {
              console.log('not found');
              sendJsonResponse(res, 404, "not found");
            }

          }
        });
  } else {
    sendJsonResponse(res, 400, 'Email address not given');
  }
};

module.exports.sendEmail = function(req, res) {
  console.log('---------------', req.body.params.email);

  var transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {

      user: 'perceptron99@gmail.com',
      pass: 'mypetname1'
    }
  });
  var mailOptions = {
    from: 'perceptron99@gmail.com',
    to: req.body.params.email,
    subject: 'Test email',
    html: '<p>That was tough</p>'
  };

  transporter.sendMail(mailOptions, function(err, info) {
    if (err) {
      console.log('Error ', err);
    } else {
      sendJsonResponse(res, 200, info);
    }
  });

};

module.exports.signupMailChimp = function(req, res) {

    var name = req.body.params.name;
    var email = req.body.params.email;


    var data = {
      members: [{
        email_address: email,
        status: 'subscribed',
        merge_fields: {
          FNAME: name,
          LNAME: name,
        }
      }]
    };

    // var postData = data;
    var postData = JSON.stringify(data);

    var options = {
      url: 'https://us3.api.mailchimp.com/3.0/lists/5114e557ec',
      method: 'POST',
      headers: {
        Authorization: 'apikey ca8ed921e08744e174172fa27332d43a-us3',
        'Content-Type': 'applicaton/json'
      },
      body: postData
    };
    

    request(options, function(err, response, body) {

      if (err) {

        console.log('error ', err);
        sendJsonResponse(res, 404, err);
      } else {
        console.log('success');
        sendJsonResponse(res, 200, 'success');
      }
    });


    };