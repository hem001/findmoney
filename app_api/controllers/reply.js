var mongoose = require('mongoose');
var ObjectId = require('mongodb').ObjectId;
var Blog = mongoose.model('Blogs');
var User = mongoose.model('User');

var sendJsonResponse = function(res, status, content) {
  res.status(status);
  res.json(content);
};

module.exports.deleteReplyById = function(req, res) {
  var blogid = req.params.blogid;
  var reviewid = req.params.reviewid;
  var replyid = req.params.replyid;
  console.log('sdafsdfds----------------', blogid, reviewid, replyid);
  if (blogid && reviewid && replyid) {
    //check if node has branches. delete only if node does not have branches and 
    // remove its id from parent's node
    Blog
      .findById(blogid)
      .select('reviews')
      .exec(
        function(err, blog) {
          if (err) {
            sendJsonResponse(res, 400, err);
          } else {
            var ids = blog.reviews.id(reviewid).replies.id(replyid).childrenids;
            // console.log("ids are",ids);

            if (ids.length) {
              console.log("Error");
              sendJsonResponse(res, 400, {
                "message": "Has children, cannot delete it."
              });
            } else {

              //delete id from parents branch
              var parentid = blog.reviews.id(reviewid).replies.id(replyid).parentid;
              console.log('parent id is ', parentid);

              var index = blog.reviews.id(reviewid).replies.id(parentid).childrenids.findIndex((a) => {
                return a == replyid;
              });
              blog.reviews.id(reviewid).replies.id(parentid).childrenids.splice(index, 1);

              blog.save(function(err) {
                if (err) {
                  sendJsonResponse(res, 400, err);
                } else {

                  blog.reviews.id(reviewid).replies.id(replyid).remove();
                  blog.save(function(err) {
                    if (err) {
                      sendJsonResponse(res, 400, err);
                    } else {


                      console.log("-----------successd1");
                    }
                  });


                  console.log("-----------successd2");
                  sendJsonResponse(res, 200, null);
                }
              });





              //can delete the reply branch

            }

          }
        }
      );


    // Blog
    //   .findById(blogid)
    //   .select('reviews')
    //   .exec(
    //     function(err, blog) {
    //       if (err) {
    //         sendJsonResponse(res, 400, err);
    //       }else if(!blog || !blog.reviews.id(reviewid) || !blog.reviews.id(reviewid).replies.id(replyid)){
    //         console.log("no blog or review");
    //         sendJsonResponse(res, 400, err);

    //       } else {
    //         console.log('help');
    //         blog.reviews.id(reviewid).replies.id(replyid).remove();
    //         blog.save(function(err) {
    //           if (err) {
    //             sendJsonResponse(res, 400, err);
    //           } else {

    //             console.log("-----------success");
    //             sendJsonResponse(res, 200, null);
    //           }
    //         });

    //       }
    //     });

  } else {
    sendJsonResponse(res, 400, {
      "message": "post not found"
    });
  }
};

module.exports.createReply = function(req, res) {

  if (req.params.blogid && req.params.reviewid) {

  var id = new ObjectId();
  // console.log("new level is ", req.body.level);
  var reply = {
    _id: id,
    name: req.body.name,
    text: req.body.text,
    date: req.body.date,
    time: req.body.time,
    level: req.body.level,
    reviewid: req.body.reviewid,
    parentid: req.body.parentid,
    image_source: req.body.image_source,
  };

  if (req.body.parentid != 0) {
    // console.log("------------", req.body.parentid);

    //push current reply's id to childrenids of parent
    Blog
      .findById(req.params.blogid)
      .select('reviews')
      .exec(
        function(err, blog) {
          if (err) {
            sendJsonResponse(res, 400, err);
          } else {
            blog.reviews.id(req.params.reviewid).replies.id(req.body.parentid)
              .childrenids.push(id);
            blog.save(function(err) {
              var thisReply;
              if (err) {
                sendJsonResponse(res, 400, err);
              } else {
                // console.log("-----------success0");
                // var l = blog.reviews.id(req.params.reviewid).replies.length-1;

                // thisReply = blog.reviews.id(req.params.reviewid).replies[l];
                
                // sendJsonResponse(res, 201, "");
              }
            });

          }
        }
      );
  }
  //push the reply
    Blog
      .findById(req.params.blogid)
      .select('reviews')
      .exec(
        function(err, blog) {
          if (err) {
            sendJsonResponse(res, 400, err);
          } else {
            blog.reviews.id(req.params.reviewid).replies.push(reply);

            blog.save(function(err) {
              if (err) {
                sendJsonResponse(res, 400, err);
              } else {
                var l = blog.reviews.id(req.params.reviewid).replies.length-1;
                // console.log("-----------success1", l);
                var thisReply = blog.reviews.id(req.params.reviewid).replies[l];
                // console.log("-----------success1", l, thisReply);
                sendJsonResponse(res, 201, thisReply);
              }
            });

          }
        }
      );


    // Blog
    //   .update({ "_id": req.params.blogid, 'reviews._id': req.params.reviewid }, {
    //     $push: {
    //       'reviews.$.replies': reply
    //     }
    //   }, { upsert: true }, function(err, reply) {
    //     if (err) {
    //       sendJsonResponse(res, 400, err);
    //     } else {
    //       console.log("oh no-------------------");

    //       sendJsonResponse(res, 201, reply);
    //     }
    //   });

  
}else {
    sendJsonResponse(res, 400, null);

}
};